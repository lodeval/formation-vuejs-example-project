import './components/Address-6.js';
import './components/AddressForm-6.js';

Vue.filter('uppercase', (value) => {
  if (!value) return '';
  return value.toUpperCase();
});

var app = new Vue({
  el: '#app',
  template: ``,
  data: {
    from: {
      road: '52 rue Jacques Babinet',
      zipCode: '31000',
      city: 'Toulouse',
      complement: 'Makina Corpus',
      country: 'Occitanie'
    }, to: {
      road: 'place du Capitole',
      zipCode: '31000',
      city: 'Toulouse',
      state: 'France'
    }
  },
})
