import Vue from 'vue';
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css' // Ensure you are using css-loader

import Address from './components/Address.vue';
import AddressForm from './components/AddressForm.js';
import './filters/uppercase.js';

import './styles/main.css';

Vue.use(Vuetify)

var app = new Vue({
  el: '#app',
  data: {
    from: {
      road: '52 rue Jacques Babinet',
      zipCode: '31000',
      city: 'Toulouse',
      complement: 'Makina Corpus',
      country: 'Occitanie'
    }, to: {
      road: 'place du Capitole',
      zipCode: '31000',
      city: 'Toulouse',
      state: 'France'
    },
    selectedAddress: '',
  },
  components: {
    'mc-address': Address
  }
})
