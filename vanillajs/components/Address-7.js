Vue.component('mc-address', {
  template: `<v-card class="mb-2">
    <v-card-title>
      <div>
      <h3>Address {{label}}</h3>
      Road : {{ address.road }}<br/>
      Zip Code : {{ address.zipCode }}<br/>
      City : {{ address.city | uppercase }}<br/>
      <span v-if="address.complement">Complement: {{ address.complement }}<br/></span>
      <span v-if="address.country">Country: {{ address.country }}<br/></span>
      <span v-if="address.state">State: {{ address.state | uppercase }}<br/></span>
      </div>
    </v-card-title>
  </v-card>`,
  props: ['address', 'label']
});
